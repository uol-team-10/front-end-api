from flask import Blueprint, request
from databaseAccess import DatabaseAccess


user_api = Blueprint('user_api', __name__)

# TODO: ASK ABOUT POSTING USERS - no way to add a user
# GET
# Retrieve user IDs up to a specified amount (if given)
@user_api.route('/users<limit>', method='GET')
def users():
    limit = request.args.get("limit")
    # TODO: PERMITTED USER ACCESS LIST, also how to retrieve a specified number of users


# Retrieve the attributes of a specified user
@user_api.route('/users/<userID>/attributes<attribute>', method=['GET', 'DELETE'])
def user_attribute(userID):
    if request.method == 'GET':
        dbAccess = DatabaseAccess()
        # TODO: ONLY RETURN SPECIFIED ATTRIBUTE IF GIVEN (else all attributes?)
        attribute = request.args.get("attribute")
        return dbAccess.user_get_info()
    elif request.method == 'DELETE':
        # TODO: Seemingly no way of achieving this with the current databaseAccess.py
        return ''


# TODO: speak to Seth regarding role access from groups, Oli also wanted for this
# Retrieve all users of a specified role
@user_api.route('/roles/users<role>', method='GET')
def user_roles(role):
    return ''


# POST
# This will create a new role
@user_api.route('/roles', method='GET')
def roles():
    return ''


# This will assign a user a role
@user_api.route('/roles/users', method='GET')
def user_roles():
    return ''


# DELETE
@user_api.route('/users/<userID>', method='DELETE')
def user_attribute(userID):
    dbAccess = DatabaseAccess()
    dbAccess.user_remove(userID)


#
@user_api.route('/roles/<role>', method='DELETE')
def roles(role):
    return ''