from flask import Blueprint, request
from databaseAccess import DatabaseAccess


groups_api = Blueprint('groups_api', __name__)


# GET: Retrieves group IDs as well as their attributes up to a specified limit (if provided)
# POST: Adds a new group
# TODO: FIX A BUNCH OF THESE, DISCUSS WITH SETH - LIMIT IMPLEMENTATION
@groups_api.route('/groups<limit><groupID>', method=['GET', 'POST'])
def group_data():
    dbAccess = DatabaseAccess()
    if request.method == 'GET':
        # TODO: add a default val for limit that ensures there can be a missing limit param
        limit = request.args.get("limit")
        return''

    elif request.method == 'POST':
        # TODO: add a format check for groupIDs, otherwise reject
        groupID = request.args.get("groupID")
        dbAccess.group_add(groupID)


# Retrieves attributes for a specific group
@groups_api.route('/groups/<groupID>', method=['GET', 'POST', 'DELETE'])
def groups(groupID):
    if request.method == 'GET':
        return ''

    elif request.method == 'POST':
        return ''

    elif request.method == 'DELETE':
        dbAccess = DatabaseAccess()
        dbAccess.group_remove(groupID)


# Returns all devices within a specific group
@groups_api.route('/groups/<groupID>/devices<limit>', method='GET')
def groups_devices(groupID):
    request.args.get("limit")

    dbAccess = DatabaseAccess()
    return dbAccess.group_get_devices(groupID)


# Returns all users in a specific group
@groups_api.route('/groups/<groupID>/users<limit>', method='GET')
def groups_users(groupID):
    request.args.get("limit")

    dbAccess = DatabaseAccess()
    return dbAccess.group_get_users(groupID)


# Retrieve all groups within a selected group
# Groups are nestable to create a tree structure
@groups_api.route('groups/<groupID>/groups<limit>', method='GET')
def nested_groups(groupID):
    request.args.get("limit")

    dbAccess = DatabaseAccess()
    return dbAccess.group_get_children(groupID)


