from flask import Blueprint, request
from databaseAccess import DatabaseAccess


device_api = Blueprint('device_api', __name__)


# GET
# Retrieves the IDs of all devices (up to a specified limit, if given)
@device_api.route('/devices<limit><deviceID><groupID><ip><token>', method=['GET', 'POST'])
def device_data():
    dbAccess = DatabaseAccess()
    if request.method == 'GET':
        # TODO; find out again if return limit possible
        limit = request.args.get("limit")

    elif request.method == 'POST':
        deviceID = request.args.get("deviceID")
        groupID = request.args.get("groupID")
        ip = request.args.get("ip")
        token = request.args.get("token")
        dbAccess.device_add(deviceID, groupID, ip, token)


# Retrieves telemetry data for the specified device
@device_api.route('/devices/<deviceID>/telemetry/<telemetry>', method='GET')
def device_telemetry(deviceID):
    return ''


# Retrieves only the LATEST telemetry points for the specified device (from the database)
@device_api.route('/devices/<device>/telemetry/latest<telemetry>', method='GET')
def latest_device_telemetry(device, telemetry):
    return ''


# Retrieves the data for the specified device within specific time frame.
# Data aggregation is possible.
@device_api.route('/devices/<device>/telemetry/timeseries<telemetry,interval,aggregation,startts,endts,limit>', method='GET')
def timeseries_device_telemetry(device, telemetry, interval, aggregation, startts, endts, limit):
    return ''


# Retrieves attributes of specified device
# TODO: ask about if an attribute param isn't provided. Return all or reject query?
@device_api.route('/devices/<device>/attributes<attribute>', method=['GET', 'DELETE'])
def device_attribute(device, attribute):
    if request.method == 'GET':
        return ''
    if request.method == 'DELETE':
        return ''


# POST
# This will change/add attributes to specified device
@device_api.route('/devices/<deviceID>', method=['POST', 'DELETE'])
def device_data(deviceID):
    dbAccess = DatabaseAccess()
    if request.method == 'POST':
        return ''

    elif request.method == 'DELETE':
        dbAccess.device_remove(deviceID)


# Wrtie a new telemetry reading to MongoDB
@device_api.route('/devices/<device>/telemetry', method='POST')
def device_telemetry(device):
    return ''



